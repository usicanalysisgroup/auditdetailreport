/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usicinc.analysis;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ShanshanChen
 */
public class AuditMail extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());//added by ming to fix class loader that is not able to see the classes from com.sun.mail.handlers when running in the context of a servlet container
            
            String template = Files.readFromFile(getServletContext().getRealPath("template.html"));
            String tableTemplate = Files.readFromFile(getServletContext().getRealPath("table.html"));
            String logoPath = getServletContext().getRealPath("USICLogo.gif");
            out.print(tableTemplate);
            Audit report = new Audit(template, tableTemplate, logoPath);
            report.genList();
            report.sendAll();
            
            out.print("MAIL SENT!");

            out.flush();
        } finally {
            out.close();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

class Audit {

    private Database sql = null;
    private Mail mails = null;

    private String template;
    private String tableTemplate;
    private String logoPath;
    
    public Audit(String template, String tableTemplate, String logoPath) {
        this.template = template;
        this.tableTemplate = tableTemplate;
        this.logoPath = logoPath;
    }
    
    public void genList() {

        sql = new Database();
        mails = new Mail("smtp.usicinc.com", "Analysis", "readeranalysis");

        Map<String, Map<Integer, String>> mailingList = getMailingList();

        for (int i = 0; mailingList.get("Zone").size() > i; i++) {
           
            //String user = mailingList.get("User").get(i);
            
            String scope = mailingList.get("Scope").get(i);
            String zone = mailingList.get("Zone").get(i);
            String date = mailingList.get("First").get(i);   
            String by = mailingList.get("By").get(i);
            String supEmail = mailingList.get("SupEmail").get(i);
            String dmEmail = mailingList.get("DMEmail").get(i);
            String supervisor= "";

            Map<String, Map<Integer, String>> content = getContent(scope, zone,date);            
            String tables = "";                                     
            for (int j = 0; content.get("Date").size() > j; j++) 
            {                
                String date1 ="";// content.get("Date").get(j);                
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");                    
                try {
                    date1 = df.format(df.parse(content.get("Date").get(j).substring(0, 10)));                         

                } catch (ParseException ex) {
                    System.out.println("error");
                }
                String team = content.get("Team").get(j);
                String sup = content.get("User").get(j);
                String locatorid = content.get("LocatorID").get(j);
                String name = content.get("Name").get(j);
                //String tickets = content.get("Cnt").get(j);
                String ticketno = content.get("TicketNumber").get(j);
                String utility = content.get("Util").get(j);
                String address = content.get("Add").get(j);
                String onsite = content.get("OnSite").get(j);
                String project = content.get("Project").get(j);
                String damagerelated = content.get("DmgIvg").get(j);
                String hp = content.get("HP").get(j);
                String arrivaldiff = content.get("Arrival Diff").get(j);
                String arrival = content.get("RptArr").get(j);
                String servicediff = content.get("Service Diff").get(j);
                String service = content.get("RptSer").get(j);
                String bill = content.get("Bill").get(j);
                String doc = content.get("Doc").get(j);
                String scopecovered = content.get("Scope").get(j);
                String markingstds = content.get("Standard").get(j);
                String locateacc = content.get("OnsiteLoc").get(j);
                String damagecode = content.get("C2Dmg").get(j);
                String reasonroute = content.get("ReasonableRoute").get(j);
                String idletime = content.get("IdleMins").get(j);
                String effiroute = content.get("EfficientRoute").get(j);
                String overall = content.get("Overall").get(j);
                String comment = content.get("Comment").get(j);
                String emergency = content.get("Emergency").get(j);
                String score = content.get("Score").get(j);
                String ticcomment = content.get("TicComment").get(j);
                                 
                String table = addTable(tableTemplate, date1,    team,  sup,  locatorid,  name,  "", ticketno,  utility,    address,  onsite,  project,  damagerelated,  hp,  arrivaldiff, arrival,
           servicediff,  service,  bill,  doc,  scopecovered,  markingstds,  locateacc,  damagecode, reasonroute,  idletime, effiroute, overall, comment,
           emergency,score,ticcomment);
                tables = tables.concat(table);
                supervisor = sup;
            }//end of each line.             
            Map<String, String> templateParams = new HashMap<String, String>();
            templateParams.put("scope", mailingList.get("Scope").get(i));
            templateParams.put("zone", mailingList.get("Zone").get(i));
            templateParams.put("name", mailingList.get("Name").get(i));
            templateParams.put("user", mailingList.get("User").get(i));
            templateParams.put("by", mailingList.get("By").get(i));
            templateParams.put("supervisor", supervisor);
            if (mailingList.get("DM").get(i)!=null){
               templateParams.put("DM",mailingList.get("DM").get(i));  
            } else {
               templateParams.put("DM","minglu");     
            }
           
            //System.err.append("Tables: "+tables);
            templateParams.put("tables", tables);
             
            //System.out.println(templateParams);
            addMail(template, templateParams, logoPath, supEmail,dmEmail);
            
            
        } //end of all emails pushing process

   }//end of genList()

 
    private Map<String, Map<Integer, String>> getMailingList() {
        return (sql.getProc("[dbo].[GPS/Audit Report GetMailingList]"));
    }
    private Map<String, Map<Integer, String>> getContent(String scope, String zone,String date) 
    {
        Map<String, String> inputParams = new HashMap<String, String>();        
        inputParams.put("@scope", scope);
        inputParams.put("@zone", zone);
        inputParams.put("@date", date);
        inputParams.put("@period", "daily");
        inputParams.put("@type", "complete");
        inputParams.put("@option", "0");
        inputParams.put("@metric", "overall");
        inputParams.put("@filter", "total");
        inputParams.put("@base", "0");        

        return (sql.getProc("[dbo].[GPS/GPS Audits Summary-Detail Report v2]", inputParams));
    }
    
    private void addMail(String template, Map<String, String> templateParams, String logoPath, String supEmail, String dmEmail) {
        String from = "Analysis@usicinc.com";
        String fromName = "Analysis";
        String email = templateParams.get("user").concat("@usicllc.com");
        String name = templateParams.get("name");
        String title = "Audit Details Report Audited by "+templateParams.get("by");
        Email e = mails.addEmail(from, fromName, email, name, title, template, templateParams);
        e.addImage(new File(logoPath), "logo");
        
        if(supEmail.equals("1")){
                String supemail =  templateParams.get("by").concat("@usicllc.com");
                String suptitle =  "Audit Details Report Copy For " + templateParams.get("name");
                 Email e2 = mails.addEmail(from, fromName, supemail, name, suptitle, template, templateParams);
                 e2.addImage(new File(logoPath), "logo");
        }
        
           if(dmEmail.equals("1")){
                String dmemail =  templateParams.get("DM").concat("@usicllc.com");
                String dmtitle =  "Audit Details Report Copy For " + templateParams.get("name");
                 Email e2 = mails.addEmail(from, fromName, dmemail, name, dmtitle, template, templateParams);
                 e2.addImage(new File(logoPath), "logo");
        }
    }
    
 
    private String addTable(String tableTemplate, String date,   String team, String sup, String locatorid, String name, String tickets,
          String ticketno, String utility,   String address, String onsite, String project, String damagerelated, String hp, String arrivaldiff,String arrival,
          String servicediff, String service, String bill, String doc, String scopecovered, String markingstds, String locateacc, String damagecode,
          String reasonroute, String idletime,String effiroute, 
          String overall,String comment,String emergency,String score,String ticcomment) {

        Map<String, String> tableTemplateParams = new HashMap<String, String>();
        tableTemplateParams.put("date", date);
        tableTemplateParams.put("team", team);
        tableTemplateParams.put("sup", sup);
        tableTemplateParams.put("locatorid", locatorid);
        tableTemplateParams.put("name", name);
        tableTemplateParams.put("tickets", tickets);
        tableTemplateParams.put("ticketno", ticketno);
        tableTemplateParams.put("utility", utility);
        tableTemplateParams.put("address", address);
        tableTemplateParams.put("onsite", onsite);
        tableTemplateParams.put("project", project);
        tableTemplateParams.put("damagerelated", damagerelated);
        tableTemplateParams.put("hp", hp);
        tableTemplateParams.put("arrivaldiff", arrivaldiff);
        tableTemplateParams.put("arrival", arrival);
        
        tableTemplateParams.put("servicediff", servicediff);
        tableTemplateParams.put("service", service);
        tableTemplateParams.put("bill", bill);
        tableTemplateParams.put("doc", doc);
        tableTemplateParams.put("scopecovered", scopecovered);
        tableTemplateParams.put("markingstds", markingstds);        
        tableTemplateParams.put("locateacc", locateacc);
        tableTemplateParams.put("damagecode", damagecode);
        tableTemplateParams.put("reasonroute", reasonroute);
        tableTemplateParams.put("idletime", idletime);
        tableTemplateParams.put("effiroute", effiroute);
        tableTemplateParams.put("overall", overall);
        tableTemplateParams.put("comment", comment); 
        
        tableTemplateParams.put("emergency", emergency); 
        tableTemplateParams.put("score", score); 
        tableTemplateParams.put("ticcomment", ticcomment); 
        return (com.usicinc.analysis.Email.doTemplate(tableTemplate, tableTemplateParams));
    }
    public void sendAll() {
        mails.sendAll();
    }    
}